#!/bin/bash -e
cd /opt/image-test

# Local nexus proxy
curl --silent --fail --location http://192.168.1.254:9081/content/groups/npm-all/ >/dev/null && (
  echo "Using Nexus mirror at http://192.168.1.254:9081/content/groups/npm-all/"
  echo >>"${HOME}/.npmrc" <<EOF
registry=http://192.168.1.254:9081/content/groups/npm-all/
EOF
)

exec make -k test
