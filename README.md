bitbucket-pipeline-browsers [![CircleCI](https://circleci.com/bb/double16/bitbucket-pipeline-browsers.svg?style=svg&circle-token=98541ded6785dc0452bd4e8c1b2c739bbaad7b94)](https://circleci.com/bb/double16/bitbucket-pipeline-browsers)
===========================
[![](https://images.microbadger.com/badges/image/pdouble16/bitbucket-pipeline-browsers.svg)](http://microbadger.com/images/pdouble16/bitbucket-pipeline-browsers "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/version/pdouble16/bitbucket-pipeline-browsers.svg)](http://microbadger.com/images/pdouble16/bitbucket-pipeline-browsers "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/commit/pdouble16/bitbucket-pipeline-browsers.svg)](http://microbadger.com/images/pdouble16/bitbucket-pipeline-browsers "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/license/pdouble16/bitbucket-pipeline-browsers.svg)](http://microbadger.com/images/pdouble16/bitbucket-pipeline-browsers "Get your own version badge on microbadger.com")

Atlassian default pipeline build image with web browsers for running functional tests. The tagging
scheme is `(base version)_(firefox version)_(chrome version)_(phantomjs version)`.

Firefox
-------
[Firefox](https://www.mozilla.org/en-US/firefox/products/) is installed at `/usr/bin/firefox`.

Google Chrome
-------------
[Chrome](http://www.google.com/chrome/) is installed at `/usr/bin/google-chrome`.

PhantomJS
---------
[PhantomJS](http://phantomjs.org) is installed at `/usr/bin/phantomjs`.

Window Resolution
-----------------
The window resolution can be set by the environment variables `SCREEN_WIDTH`, `SCREEN_HEIGHT`, `SCREEN_DEPTH`. The
default resolution is `1360x1020x24`.

WebDriver
---------
If you're using WebDriver for your tests, configure it according to the standard documentation. The browsers are
available in the system path with their usual names, so the driver should pick them up. If you need to specify the path
to the browser binary, see above for each browser.

For example, see the Selenium documentation at [SeleniumHQ](http://docs.seleniumhq.org/docs/03_webdriver.jsp#firefox-driver).

