selenium = require 'selenium-webdriver'
chai = require 'chai'
chai.use require 'chai-as-promised'
expect = chai.expect

before ->
  @timeout 90000
  @driver = new selenium.Builder()
    .forBrowser('firefox')
    .build()
  @driver.getWindowHandle()

after ->
  @driver.quit()

describe 'www.google.com', ->
  beforeEach ->
    @driver.get 'https://www.google.com/'

  it 'has the Google Search page title in the window\'s title', ->
    expect(@driver.getTitle()).to.eventually.contain 'Google'

