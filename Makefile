IMAGE_NAME = guroos/bitbucket-pipeline-browsers
TIMEOUT = 90000
REPORTER = xunit
#BROWSERS := chrome firefox phantomjs
# Selenium has dropped support for phantomjs
BROWSERS := chrome firefox

REPORTER_DIR := ${CIRCLE_TEST_REPORTS}
ifeq (,$(REPORTER_DIR))
  REPORTER_DIR := $(shell pwd)/test/results
endif

.PHONY: build test npminstall $(BROWSERS)

build:
	docker build -t $(IMAGE_NAME) .

test: $(BROWSERS) $(REPORTER_DIR)
	! cat $(REPORTER_DIR)/*.xml | grep -o '\(failures\|errors\)="[1-9][0-9]*"'

${REPORTER_DIR}:
	mkdir -p $(REPORTER_DIR)

npminstall:
	cd test ; npm install

$(BROWSERS): $(REPORTER_DIR)
	node --version
	cd test ; SELENIUM_BROWSER=$@ PATH=./node_modules/.bin:${PATH} mocha test.coffee --compilers coffee:coffeescript/register --timeout $(TIMEOUT) --reporter $(REPORTER) | tee $(REPORTER_DIR)/TEST-$@.xml
